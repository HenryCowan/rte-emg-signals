# RTE - EMG Signals

A series of EMG recordings used as test signals for the real-time embedded systems project at the university of glasgow.

Example python code included for plotting a single channel recording.
