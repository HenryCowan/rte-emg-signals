# -*- coding: utf-8 -*-
"""
Spyder Editor

Henry Cowan
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import iirfilter, lfilter


UnFilteredDataArray = np.loadtxt("...rte-emg-signals/SingleChannel/Mixed2.dat")

UnFilteredData = UnFilteredDataArray[:,1]

def Implement_Notch_Filter(fs, band, freq, ripple, order, filter_type, data):

    nyq  = fs/2.0
    low  = freq - band/2.0
    high = freq + band/2.0
    low  = low/nyq
    high = high/nyq
    b, a = iirfilter(order, [low, high], rp=ripple, btype='bandstop',
                     analog=False, ftype=filter_type)
    filtered_data = lfilter(b, a, data)
    return filtered_data

FilteredData50Hz = Implement_Notch_Filter(1000,1,50,2,2,'butter',UnFilteredData)
FilteredDataBaseline = Implement_Notch_Filter(1000,2,1,2,2,'butter',FilteredData50Hz)

plt.subplot(411)
plt.plot(FilteredData50Hz)
plt.subplot(412)
plt.plot(abs(np.fft.fft(FilteredData50Hz)))
plt.subplot(413)
plt.plot(FilteredDataBaseline)
plt.subplot(414)
plt.plot(abs(np.fft.fft(FilteredDataBaseline)))
